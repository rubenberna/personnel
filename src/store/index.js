import Vue from 'vue';
import Vuex from 'vuex';
import personnel from './modules/personnel';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        personnel
    }
});