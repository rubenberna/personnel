import { router } from '../../main';

const state = {
    data: []
};

const getters = {
    allFishers: state => state
};

const actions = {
    // add fishtripr
    addMember(context, person) {
        context.commit('addToList', person);
    },
    clearMembers(context) {
        context.commit('clearList');
    }
};

const mutations = {
    // commit action to state
    addToList(state, person) {
    state.data.push(person);
},
    clearList(state) {
        state.data = [];
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};


/*
POSSIBLE ERRORS:
- Submit button is not submitting, but just keeping the v-model data;
- Push function in mutation not working;
- 
*/