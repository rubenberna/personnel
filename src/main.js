import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import SuiVue from 'semantic-ui-vue';
import store from './store/index.js';
import PersonnelList from './components/PersonnelList';


Vue.use(SuiVue);

Vue.use(VueRouter);

export const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: PersonnelList }
  ]
});

new Vue({
  store,
  router,
  el: '#app',
  render: h => h(App)
})
